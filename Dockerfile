FROM python:3.6-alpine

RUN apk update

COPY ./requirements.txt /requirements.txt


RUN mkdir /src
WORKDIR /src
COPY . /src/

RUN pip install -r requirements.txt
RUN python manage.py migrate

CMD ["gunicorn", "penpal.wsgi:application", "--bind", "0.0.0.0:8000", "--workers", "4"]
